import React from "react";
import { StyleSheet, Text, View, Button, TextInput } from "react-native";
import { Provider } from "react-redux";
import { createStore } from "redux";
import rootReducer from "../Reducers";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as firebase from "firebase";
import { YellowBox } from 'react-native';
import _ from 'lodash';

YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};

/**
 * Import local dependencies.
 */
import GameSetup from "./gameSetup.js";
import GamePlay from "./gamePlay.js";
import { fetchData } from "../Actions/apiCall.js";
import fbdb from "../Firebase";



class Screen extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    // this.props.fetchData();
    
  }


  render() {
    if (this.props.realgameData.GameReady === true) {
      return <GamePlay />
    } else {
      return <GameSetup />
    }

  }
}

const mapStateToProps = state => {
  return { activeCard: state.activeCard, realgameData: state.gameData };
};

// const mapDispatchToProps = dispatch => {
//   return {
//     fetchData: bindActionCreators(fetchData, dispatch)
//   };
// };

export default connect(mapStateToProps)(Screen);

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
    backgroundColor: "#fff",
    padding: 15
  },
  welcome: {
    fontSize: 21,
    marginBottom: 10
  },
  plotDisplay: {
    padding: 10,
    borderColor: "#ccc",
    borderWidth: 1,
    textAlign: "center"
  }
});
