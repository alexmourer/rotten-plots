import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { Provider } from "react-redux";
import { createStore } from "redux";
import rootReducer from "../Reducers";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as firebase from "firebase";
import { YellowBox } from 'react-native';
import _ from 'lodash';

YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
    if (message.indexOf('Setting a timer') <= -1) {
        _console.warn(message);
    }
};

/**
 * Import local dependencies.
 */
import CardStructure from "../Components/GamePlay/CardStructure";
// import { fetchData } from "../Actions/apiCall.js";
import fbdb from "../Firebase";
import { genStore } from "../Actions";
import GamePlay from "../Components/GamePlay";
import RoundResults from "../Components/RoundResults";


class Screen extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
       
        // this.gameWatch()
        // this.props.fetchData();
    }

    render() {
        const genStore = this.props.generalStore;
        const uid = this.props.realgameData.PlayerNumber;
        const turnPlayed = "Player" + uid;
        // console.log(genStore.Player1);
        return (
            <View style={styles.container}>
            {genStore[turnPlayed] ? (
                <RoundResults />
              ) : (
                <GamePlay />
              )}

           
               
            </View>
        );
    }
}

const mapStateToProps = state => {
    return { activeCard: state.activeCard, CardData: state.CardData, realgameData: state.gameData, generalStore: state.genStore, };
};

const mapDispatchToProps = dispatch => {
    return {
        genStore: bindActionCreators(genStore, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Screen);

const styles = StyleSheet.create({

});
