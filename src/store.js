import { applyMiddleware, createStore } from "redux";
import rootReducer from "./Reducers";
// import logger from "redux-logger";
import thunkMiddleware from "redux-thunk";

export default () => {
  return createStore(rootReducer, applyMiddleware(thunkMiddleware));
};
