// import { CHANGE_DATA } from "../Actions/apiCall";

// let initialState = [];

// export default function(state = [], action) {
//   console.log("pay " + action.dataload);
//   switch (action.type) {
//     case CHANGE_DATA:
//       return [action.dataload, ...state];
//   }
//   return state;
// }
// export default (apiData = (state = {}, action) => {
//   switch (action.type) {
//     case CHANGE_DATA:
//       return [...state, action.dataload];
//     default:
//       return state;
//   }
// });


// export default (apiData = (state = {}, action) => {
//   switch (action.type) {
//     case action.type: {
//       var base = action.type;
//       state = { ...state, [base]: action.payload };
//       break;
//     }
//   }
//   return state;
// });



import { API_ACTION } from "../Actions/apiCall";

export default (apiData = (state = {}, action) => {
    switch (action.type) {
      case "protagonist": {
        state = { ...state, protagonist: action.dataload  };
        break;
      }
      case "trait": {
        state = { ...state, trait: action.dataload };
        break;
      }
      case "first": {
        state = { ...state, first: action.dataload };
        break;
      }
      case "antagonist": {
        state = { ...state, antagonist: action.dataload};
        break;
      }
      case "final": {
        state = { ...state, final: action.dataload};
        break;
      }
    }
    return state;
  });
  