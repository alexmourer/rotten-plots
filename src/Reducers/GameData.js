import { GAME_ACTION } from "../Actions/gameData";

let initialState = [];
export default (gameData = (state = {}, action) => {
  switch (action.type) {
    case GAME_ACTION: {
      state = {...state, GameID: action.id, GameReady: action.ready, PlayerNumber: action.playerNum};
      break;
    }  
  }
  return state;
});
