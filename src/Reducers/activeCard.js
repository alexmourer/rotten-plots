export default (activeCard = (state = {}, action) => {
  switch (action.type) {
    case "protagonistCard": {
      state = { ...state, protagonistCard: action.payload  };
      break;
    }
    case "traitCard": {
      state = { ...state, traitCard: action.payload };
      break;
    }
    case "firstCard": {
      state = { ...state, firstCard: action.payload };
      break;
    }
    case "antagonistCard": {
      state = { ...state, antagonistCard: action.payload};
      break;
    }
    case "finalCard": {
      state = { ...state, finalCard: action.payload};
      break;
    }
  }
  return state;
});
