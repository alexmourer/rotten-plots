import { combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import apiData from "./apiData.js";
import CardData from "./CardData.js";
import activeCard from "./activeCard.js";
import gameData from "./GameData.js";
import genStore from "./base.js";

const allReducers = combineReducers({
  apiData: apiData,
  // CardData: apiData,
  activeCard: activeCard,
  gameData: gameData,
  genStore: genStore,
});

export default allReducers;
