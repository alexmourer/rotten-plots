import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { Provider } from "react-redux";
import { createStore } from "redux";
import rootReducer from "../../Reducers";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as firebase from "firebase";
import { YellowBox } from 'react-native';
import _ from 'lodash';

YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
    if (message.indexOf('Setting a timer') <= -1) {
        _console.warn(message);
    }
};

/**
 * Import local dependencies.
 */
import CardStructure from "./CardStructure";
// import { fetchData } from "../../Actions/apiCall.js";
import fbdb from "../../Firebase";
import { genStore } from "../../Actions";


class GamePlay extends React.Component {

    constructor(props) {
        super(props);
        this.state = {roundPlayed: false};
    }

    componentDidMount() {
        
        this.gameWatch()
        // this.props.fetchData();
        console.log(this.props.activeCard)
    }

    gameWatch = () => {
     const ref = firebase.database().ref('Game/' + this.props.realgameData.GameID + "/Players");
      ref.on("child_changed", gameChange = (snapshot) => {
        var gameChanged = snapshot.val();
        
        for (let i = 0; i < gameChanged.Rounds.length; i++) {
            var n = i + 1;
            this.props.genStore("CurrentRound", n);
            if(gameChanged.Rounds[i].Complete === true){
                console.log("true")
                this.props.genStore("Player" + gameChanged.UID, gameChanged.Username + "'s Turn Complete");
                
                break;
            }
        }
        
      });
    
   }

    _addItem = () => {
        const storeBase = this.props.generalStore;
        const ref = firebase.database().ref('Game/' + this.props.realgameData.GameID);
        return ref.once('value').then(gamePlay = (snapshot) => {
            var gameData = (snapshot.val());
            for (let i = 0; i < gameData.Players.length; i++) {
                if(gameData.Players[i].Username == this.props.generalStore.User) {
                    var roundCount = gameData.Players[i].Rounds.length;
                    var turnAdd = { 
                        [roundCount] : {
                            Card1: storeBase.protagonistCardID,
                            Card2: storeBase.traitCardID,
                            Card3: storeBase.firstCardID,
                            Card4: storeBase.antagonistCardID,
                            Card5: storeBase.finalCardID,
                            Complete: true,
                            Winner: false
                            }  
                        };
                    firebase.database().ref('Game/' + this.props.realgameData.GameID + '/Players/' + i + '/').child('Rounds').update(turnAdd);
                    break; 
                }
            }
        });

    }

    render() {
        
        const genStore = this.props.generalStore;
        
        const uid = this.props.realgameData.PlayerNumber;
        const turnPlayed = "Player" + uid;
        console.log(genStore[turnPlayed])
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>Game ID: {this.props.realgameData.GameID}</Text>
                
                
                <Text style={styles.hasPlayed}>{this.props.generalStore.Player0}</Text>
                <Text style={styles.hasPlayed}>{this.props.generalStore.Player1}</Text>
                <Text style={styles.hasPlayed}>{this.props.generalStore.Player2}</Text>
                <Text style={styles.hasPlayed}>{this.props.generalStore.Player3}</Text>
                <Text style={styles.plotDisplay}>
                    {this.props.generalStore.protagonistCard}
                    {this.props.generalStore.traitCard}
                    {this.props.generalStore.firstCard}
                    {this.props.generalStore.antagonistCard}
                    {this.props.generalStore.finalCard}
                </Text>
                {/* <Text>{this.props.CardData}</Text> */}
                <CardStructure type="protagonist" amount="4" />
                <CardStructure type="trait" amount="4" />
                <CardStructure type="first" amount="4" />
                <CardStructure type="antagonist" amount="4" />
                <CardStructure type="final" amount="4" />
                <Button title="Confirm Plot" onPress={() => this._addItem()}>
                </Button>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return { dataAPI: state.apiData, activeCard: state.activeCard, CardData: state.CardData, realgameData: state.gameData, generalStore: state.genStore, };
};

const mapDispatchToProps = dispatch => {
    return {
        genStore: bindActionCreators(genStore, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(GamePlay);

const styles = StyleSheet.create({
    container: {
        marginTop: 30,
        backgroundColor: "#fff",
        padding: 15
    },
    welcome: {
        fontSize: 21,
        marginBottom: 10
    },
    plotDisplay: {
        padding: 10,
        borderColor: "#ccc",
        borderWidth: 1,
        textAlign: "center"
    },
    hasPlayed: {
        color: "#48B952"
    }
});
