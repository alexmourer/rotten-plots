import React from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableHighlight
} from "react-native";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { activeCard } from "../../Actions/cardData.js";
import { genStore } from "../../Actions";

class CardStructure extends React.Component {
  constructor(props) {
    super(props);
    this.cardRender = this.cardRender.bind(this);
    this.state = {
      CardInfo: "",
      [this.props.type]: "",
      styleIndex: ""
    };
  }

  componentDidMount() {
    let type = this.props.type;
    let typeForm = this.props.type
      .toLowerCase()
      .replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
      });
    let amount = this.props.amount;

    var url =
      "https://firebasestorage.googleapis.com/v0/b/rotten-plots.appspot.com/o/" +
      type +
      ".json?alt=media";

    fetch(url)
      .then(res => res.json())
      .then(res => {
        var cardCount = res[typeForm].length;
        var rand = function() {
          return Math.floor(Math.random() * cardCount);
        };
        var cardOne = res[typeForm][rand()];
        var cardContainer = [];
        for (let index = 0; index < amount; ) {
          var card = rand();
          if (cardContainer.includes(card)) {
          } else {
            cardContainer.push(card);
            this.setState(prevState => ({
              CardInfo: [...prevState.CardInfo, res[typeForm][card]]
            }));
            index++;
          }
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  cardSelect = (item) => {
    var type = this.props.type + "Card";
    var name = item.Name;
    
    this.props.genStore(type +"ID", item.ID);
    this.props.genStore(type, name);
    this.setState({ styleIndex: item.Name });
    
    
  }

  cardRender({ item, index }) {
    
    return (
      <TouchableHighlight
        onPress={() => this.cardSelect(item)}
        underlayColor="#48B952"
        style={
          this.state.styleIndex !== item.Name ? styles.card : styles.cardPress
        }
      >
        <View>
          <Text
            style={
              this.state.styleIndex !== item.Name
                ? styles.darkText
                : styles.whiteText
            }
          >
            {item.Name} {item.Suffix}
          </Text>
        </View>
      </TouchableHighlight>
    );
  }

  render() {
    
      return (
        <View style={styles.container}>
          <FlatList
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            automaticallyAdjustContentInsets={true}
            data={this.state.CardInfo}
            renderItem={this.cardRender}
            extraData={this.state}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      );
    
  }
}

const mapStateToProps = state => {
  return { generalStore: state.genStore, };
};
const mapDispatchToProps = dispatch => {
  return {
    genStore: bindActionCreators(genStore, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CardStructure);

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff"
  },
  card: {
    borderWidth: 1,
    borderColor: "#ccc",
    margin: 10,
    padding: 8
  },
  cardPress: {
    backgroundColor: "#48B952",
    margin: 10,
    padding: 8
  },
  darkText: {
    color: "#222"
  },
  whiteText: {
    color: "#f2f2f2"
  }
});
