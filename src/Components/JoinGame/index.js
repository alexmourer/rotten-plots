import React from "react";
import { StyleSheet, Text, View, Button, TextInput } from "react-native";
import { Provider } from "react-redux";
import { createStore } from "redux";
import rootReducer from "../../Reducers";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as firebase from "firebase";
import { YellowBox } from 'react-native';
import _ from 'lodash';

YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};

/**
 * Import local dependencies.
 */
import fbdb from "../../Firebase";
import { gameData } from "../../Actions/gameData.js";
import { genStore } from "../../Actions";
import { apiData } from "../../Actions/apiCall.js";

class JoinGame extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = { username: '', gameID:'', GameReady: false, playerNumber: '', player0: "Waiting...", player1: "Waiting...", player2: "Waiting...", player3: "Waiting..." };
    this._isMounted = false;
  }

  componentDidMount() {
    this._isMounted = true;
    this.props.apiData("protagonist");
    this.props.apiData("trait");
    this.props.apiData("first");
    this.props.apiData("antagonist");
    this.props.apiData("final");
    
    
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  gameWatch = () => {
    
    
     const ref = firebase.database().ref('Game/' + this.state.gameID);
      ref.on("child_changed", gameChange = (snapshot) => {
        if (this._isMounted === true) {
          var gameChanged = snapshot.val();
          var plyrAmnt = _.size(gameChanged.Players);
          for (let i = 0; i < plyrAmnt; i++) {
              var PlayerVar = "player" + i;
              this.setState({
                  [PlayerVar] : gameChanged.Players[i].Username 
              })
          }
          //  console.log(this.state)
          this.setState({
            GameReady: gameChanged.GameReady
          })

          var user = "User";
          this.props.genStore(user, this.state.username);
              
          this.props.gameData(this.state.gameID, this.state.GameReady, this.state.playerNumber )
        }
      });
    
   }
    

  _addItem = (username) => {
      
    _handleError = (errorMessage) => {
        // console.log(errorMessage);
    }
    
    this.gameWatch()
    // console.log(this.props.dataAPI.protagonist.Protagonist[1])
    return firebase.database().ref().once('value').then(gameSetup = (snapshot) => {
      
      
      var gameData = (snapshot.val());
      var sessionAmount = _.size(gameData);
      var gameSession = "";
      var rand = Math.floor(Math.random() * 20);
      var gameStruc = {
        CurrentRound: 0,
        Players: "",
        CardsDealt: "0",
        GameReady: false,
        Private: false
      };
      this.props.genStore("CurrentRound", "1");
      var previousData = gameData.Game;      
      var maxPlayers = 4;
      var lastPlayer = maxPlayers - 1;
      if (sessionAmount > 0){
        //check for an open game
        for (const [key, value] of Object.entries(previousData)) {
            this.setState({gameID : key});
          var playerCount = value.Players.length;
          var playerAdd = { 
            [playerCount] : {
              Rounds: "",
              Score: 0,
              Username: username,
              UID: playerCount,
              Director: false,
              Winner: false
              }  
            };
          if (playerCount < maxPlayers){
            firebase.database().ref('Game/' + key).child('Players').update(playerAdd);
            for (let i = 0; i <= value.Players.length; i++) {
                this.setState({playerNumber : i});
                var playerVar = "player" + i;
                var n = i+1
                if (i == 0) {
                    var curPlayer = "player0" ;
                }else {
                    var curPlayer = "player" + n;
                }
                
                
                if (i == lastPlayer) {
                  var gameReady = {
                    GameReady: true
                  };

                  firebase.database().ref('Game/' + key).update(gameReady);
                  firebase.database().ref('Game').push(gameStruc);
                }
            }
            
            this.props.gameData(this.state.gameID, this.state.GameReady, this.state.playerNumber )
            
            break; 
          }
        }   

      } else {
        //firebase.database().ref('Game').set();
        firebase.database().ref('Game').push(gameStruc);
      }
      
    }).catch(function(error) {
        _handleError(`There has been a problem with your fetch operation: ${error.message}`);
      });
  }

  render() {
    // console.log(this.props.activeCard);
    
    return (
      <View style={styles.container}>
        <TextInput
          style={{ height: 40 }}
          placeholder="Enter a username"
          onChangeText={(username) => this.setState({ username })}
        />
        <Button title="Play Rotten Plots" onPress={() => this._addItem(this.state.username)}>
        </Button>
        
        <Text>Player 1: {this.state.player0}</Text>
        <Text>Player 2: {this.state.player1}</Text>
        <Text>Player 3: {this.state.player2}</Text>
        <Text>Player 4: {this.state.player3}</Text>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return { dataAPI: state.apiData, realgameData: state.gameData, generalStore: state.genStore,};
};

const mapDispatchToProps = dispatch => {
  return {
    apiData: bindActionCreators(apiData, dispatch),
    gameData: bindActionCreators(gameData, dispatch),
    genStore: bindActionCreators(genStore, dispatch)
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(JoinGame);

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
    backgroundColor: "#fff",
    padding: 15
  },
  welcome: {
    fontSize: 21,
    marginBottom: 10
  },
  plotDisplay: {
    padding: 10,
    borderColor: "#ccc",
    borderWidth: 1,
    textAlign: "center"
  }
});
