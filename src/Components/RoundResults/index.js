import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { Provider } from "react-redux";
import { createStore } from "redux";
import rootReducer from "../../Reducers";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as firebase from "firebase";
import { YellowBox } from 'react-native';
import _ from 'lodash';

YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
    if (message.indexOf('Setting a timer') <= -1) {
        _console.warn(message);
    }
};

/**
 * Import local dependencies.
 */
// import CardStructure from "../GamePlay/CardStructure";
// import { fetchData } from "../../Actions/apiCall.js";
import fbdb from "../../Firebase";
import { genStore } from "../../Actions";


class GamePlay extends React.Component {

    constructor(props) {
        super(props);
        this.state = {Player1Card1: ""};
    }

    componentDidMount() {
        this.roundRender()
        this.gameWatch()
        // this.props.fetchData();
    }

    gameWatch = () => {
     const ref = firebase.database().ref('Game/' + this.props.realgameData.GameID + "/Players");
      ref.on("child_changed", gameChange = (snapshot) => {
        var gameChanged = snapshot.val();
        this.roundRender()
        // for (let i = 0; i < gameChanged.Rounds.length; i++) {
        //     // if(gameChanged.Rounds[i].Complete === true){
        //     //     this.props.genStore("Player" + gameChanged.UID, gameChanged.Rounds[i].Complete);
        //     // }
        // }
        
      });
    
    }

    roundRender = () => {
    const ref = firebase.database().ref('Game/' + this.props.realgameData.GameID + "/Players");
    ref.once('value').then(gamePlay = (snapshot) => {
        var gameResults = snapshot.val();
        for (let i = 0; i < gameResults.length; i++) {
            var player = "Player" + i;
            var Card1 = player + "Card1";
            var Card2 = player + "Card2";
            var Card3 = player + "Card3";
            var Card4 = player + "Card4";
            var Card5 = player + "Card5";
            const api = this.props.dataAPI;
            var curRound = gameResults[i].Rounds.length;
            // if (curRound > 0) {
                const thisRound = curRound-1;
                const roundData = gameResults[i].Rounds[thisRound];

                // this.setState({[Card1] : api.protagonist.Protagonist[roundData.Card1].Name });
                // this.setState({[Card2] : api.trait.Trait[roundData.Card2].Name });
                // this.setState({[Card3] : api.first.First[roundData.Card3].Name });
                // this.setState({[Card4] : api.antagonist.Antagonist[roundData.Card4].Name });
                // this.setState({[Card5] : api.final.Final[roundData.Card5].Name });

                // this.props.genStore([Card1], api.protagonist.Protagonist[roundData+"Card1"]);

            // } 

        }
      });
   }

    render() {
        console.log(this.props.generalStore)
    //   if(this.state.Player1 !== undefined) {
          
    //       var p1c1 = this.state.Player1.Card1;
    //       var p1c2 = this.state.Player1.Card2;
    //       var p1c3 = this.state.Player1.Card3;
    //       var p1c4 = this.state.Player1.Card4;
    //       var p1c5 = this.state.Player1.Card5;
    //       console.log(p1c1 + p1c2 + p1c3)
    //   }
      
     
    this.roundRender()
        return (
            <View style={styles.container}>
              
                <Text style={styles.welcome}>Round {this.props.generalStore.CurrentRound} Results</Text>
                <Text style={styles.plotDisplay}>
                    {/* {this.props.generalStore.protagonistCard}
                    {this.props.generalStore.traitCard}
                    {this.props.generalStore.firstCard}
                    {this.props.generalStore.antagonistCard}
                    {this.props.generalStore.finalCard} */}
                </Text>
                <View>
                    <Text>{this.props.generalStore.Player3Card1}</Text>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return { dataAPI: state.apiData, realgameData: state.gameData, generalStore: state.genStore, };
};

const mapDispatchToProps = dispatch => {
    return {
        genStore: bindActionCreators(genStore, dispatch)
    };
};

export default connect(mapStateToProps)(GamePlay);

const styles = StyleSheet.create({
    container: {
        marginTop: 30,
        backgroundColor: "#fff",
        padding: 15
    },
    welcome: {
        fontSize: 21,
        marginBottom: 10
    },
    plotDisplay: {
        padding: 10,
        borderColor: "#ccc",
        borderWidth: 1,
        textAlign: "center"
    },
    hasPlayed: {
        color: "#48B952"
    }
});
