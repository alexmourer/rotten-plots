export const GAME_ACTION = "GAME_ACTION";

export function gameData(id, ready, playerNum) {

  return dispatch => {
        dispatch({ type: GAME_ACTION, id: id, ready: ready, playerNum: playerNum });
  };
}
