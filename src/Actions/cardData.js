export const activeCard = (item, type) => {
  return {
    type: type,
    payload: item.Name + " " + item.Suffix + " ",
    id: item.ID
  };
};
