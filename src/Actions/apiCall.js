export const API_ACTION = "API_ACTION";

export const apiData = (card) => {
  return dispatch => {
    const url =
    "https://firebasestorage.googleapis.com/v0/b/rotten-plots.appspot.com/o/"+card+".json?alt=media";
      fetch(url)
        .then(res => {
          return res.json();
        })
        .then(response => {
          dispatch({ type: card, dataload: response });
        });

  };
}