import * as firebase from "firebase";

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyCk4QUNFj03DAP-53syky10ZDR5JpB1ivU",
  authDomain: "rotten-plots.firebaseapp.com",
  databaseURL: "https://rotten-plots.firebaseio.com",
  projectId: "rotten-plots",
  storageBucket: "rotten-plots.appspot.com",
  messagingSenderId: "362633114154"
};
firebase.initializeApp(firebaseConfig);

export const fbdb = firebase.database();