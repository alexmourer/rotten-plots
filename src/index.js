import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Provider } from "react-redux";
import { createStore } from "redux";

/**
 * Import local dependencies.
 */
import rootReducer from "./Reducers";
import Screen from "./Screens";
import Store from "./store";

const store = Store();

store.subscribe(() => {
  // console.log("store changed", store.getState());
  // console.log(store.getState().apiData.protagonist.Protagonist[0]);
});

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Screen />
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
    backgroundColor: "#fff",
    padding: 15
  },
  welcome: {
    fontSize: 21,
    marginBottom: 10
  }
});
